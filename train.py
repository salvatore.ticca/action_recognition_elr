import cv2
import os
import numpy as np
import pickle as pkl
import math
import time
import shutil
import psutil
from config import Config
from MSN import MSN
from utils import *
from tfrecords_utils import prepare_dataset
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()

def split_video(video,window_size,overlap=8):
    if(len(video.shape)>0):
        batch=[]
        n=len(video)
        if(n<window_size):
            idx=[i%n for i in range(window_size)]
            batch.append(video[idx])
        else:   
            clip=0
            for clip in range(window_size,len(video),window_size-overlap):
                batch.append(video[clip-window_size:clip])
               
            if(clip<n):
                batch.append(video[n-window_size:])
        return np.array(batch)

def run_epoch_sc(sess,iterator_lr,iterator_hr,model,lr=0.0,training=True,max_steps=None,overlap=0,verbose=False,drp_fc1=0.0,drp_fc2=0.0,drp_conv=0.0):
    epoch_loss=np.zeros(shape=(2))
    epoch_acc=np.zeros(shape=(2))
    count=0
    
    if not training:
        drp_fc1=drp_fc2=drp_conv=0

    start=time.time()
    
    results=[]
    print('dropout: fc1: {} - fc2 {} - conv: {}'.format(drp_fc1,drp_fc2, drp_conv))
    while True:
        try:

            feed_dict={
                       model.training:training,
                       model.dropout_fc1:drp_fc1,
                       model.dropout_fc2:drp_fc2,
                       model.dropout_conv:drp_conv,
                       model.learning_rate:lr}
                       
            #generate batch and give it to the model
            if(training and count%2==0):
                frames,flow,fg_mask,labels_id,label_name,video_name=sess.run(iterator_hr)#hr:0
            else:
                frames,flow,fg_mask,labels_id,label_name,video_name=sess.run(iterator_lr)#lr:1

            if(model.mask_stream):
                fg_mask = fg_mask if training else split_video(fg_mask,window_size=model.fg_mask.shape[1],overlap=overlap)
                feed_dict[model.fg_mask]=fg_mask

            if(model.rgb_stream):
                frames = frames if training else split_video(frames,window_size=model.rgb.shape[1],overlap=overlap)
                feed_dict[model.rgb]=frames
            
            if(model.optical_flow_stream):
                flow = flow if training else split_video(flow,window_size=model.flow.shape[1],overlap=overlap)
                feed_dict[model.flow]=flow

            if(training==False):
                n=frames.shape[0] if model.rgb_stream else (flow.shape[0] if model.optical_flow_stream else fg_mask.shape[0])
                labels_id=np.ones(n,dtype=np.int32)*labels_id
                
            feed_dict[model.fine_labels]=labels_id

            if(training):
                if(count%2==0):
                    loss,accuracy,_,preds=sess.run([model.loss_hr,model.accuracy_hr,model.train_op_hr,model.predictions_hr],feed_dict=feed_dict)
                else:
                    loss,accuracy,_,preds=sess.run([model.loss_lr,model.accuracy_lr,model.train_op_lr,model.predictions_lr],feed_dict=feed_dict)

                epoch_loss[count%2]+=loss
                epoch_acc[count%2]+=accuracy
            else:
                loss,preds=sess.run([model.loss_lr,model.predictions_eval],feed_dict=feed_dict)
                accuracy=1. if preds == labels_id[0] else 0.
                epoch_loss[1]+=loss
                epoch_acc[1]+=accuracy
                results.append([video_name.decode('utf-8').rsplit('/',1)[1],label_name.decode('utf-8'),str(labels_id[0]),str(preds),str(accuracy)])

            count+=1
            
            if(not max_steps is None and count>=max_steps):
                print('stopping epoch after {} updates'.format(count))
                break
                        
        except tf.errors.OutOfRangeError:
            break
            
    n=int(count/2) if training else count
    print('tot iterations: ',count,'n',n)
    
    return epoch_loss/n,epoch_acc/n,results

def run_inference_test(sess,iterator_lr,model,max_steps=None,overlap=0,verbose=False):
    inf_time=0.0
    count=0
    
    drp_fc1=drp_fc2=drp_conv=0

    start=time.time()
    
    results=[]
    print('dropout: fc1: {} - fc2 {} - conv: {}'.format(drp_fc1,drp_fc2, drp_conv))
    while True:
        try:
            feed_dict={
                       model.training:False,
                       model.dropout_fc1:drp_fc1,
                       model.dropout_fc2:drp_fc2,
                       model.dropout_conv:drp_conv,
                       model.learning_rate:0.0}
                       
            frames,flow,fg_mask,labels_id,label_name,video_name=sess.run(iterator_lr)
            if(model.mask_stream):
                fg_mask = np.expand_dims(split_video(fg_mask,window_size=model.fg_mask.shape[1],overlap=overlap)[0],axis=0)
                feed_dict[model.fg_mask]=fg_mask

            if(model.rgb_stream):
                frames = np.expand_dims(split_video(frames,window_size=model.rgb.shape[1],overlap=overlap)[0],axis=0)
                feed_dict[model.rgb]=frames
            
            if(model.optical_flow_stream):
                flow = np.expand_dims(split_video(flow,window_size=model.flow.shape[1],overlap=overlap)[0],axis=0)
                feed_dict[model.flow]=flow
    
            n=frames.shape[0] if model.rgb_stream else (flow.shape[0] if model.optical_flow_stream else fg_mask.shape[0])
            labels_id=np.ones(n,dtype=np.int32)*labels_id
            
            feed_dict[model.fine_labels]=labels_id
            
            start_time=time.time()
            loss,preds=sess.run([model.loss_lr,model.predictions_eval],feed_dict=feed_dict)
            inf_time+=time.time()-start_time
            count+=1
            if(not max_steps is None and count>=max_steps):
                print('stopping epoch after {} updates'.format(count))
                break
                        
        except tf.errors.OutOfRangeError:
            break
            
    print('tot iterations: ',count)
    
    return inf_time/count

def measure_inference_time(model,dataset,logs_dir,continue_training,max_epoch_steps):
    (trainset_iter_lr,trainset_iter_hr,valset_iter)=dataset

    with tf.Session() as sess:
        checkpoints=[f for f in os.listdir(logs_dir) if f.startswith('checkpoint_')]
        if(continue_training and len(checkpoints)>0 ):
            last_checkpoint= max(checkpoints)
            last_checkpoint = os.path.join(logs_dir,last_checkpoint,'checkpoint.ckpt')
            model.saver.restore(sess,last_checkpoint)
            print('Checkpoint restored from: ',last_checkpoint)
        else:
            print('No checkpoint found. Aborting')
            return

    
        sess.run(tf.global_variables_initializer())
        avg=0
        print('running a warm up iteration...')
        avg_time=run_inference_test(sess,valset_iter,model,max_steps=max_epoch_steps[1])
        print('average_time: ',avg_time)
        for i in range(5):
            print('-'*50)
            print('repetition ',i+1)
            avg_time=run_inference_test(sess,valset_iter,model,max_steps=max_epoch_steps[1])
            print('average_time: ', avg_time)
            avg+=avg_time

        print('-'*50)
        print('REPETITIONS AVERAGE: ',avg/5)
    
def train_sc(model,dataset,logs_dir,num_epochs=40,eval_freq=5,continue_training=False,save_freq=2,start_lr=5e-2,decay_steps=10,end_lr=5e-6,max_epoch_steps=None,verbose=False,drp_fc1=0.0,drp_fc2=0.0,drp_conv=0.0,device_verbose=False):
    tb_dir=os.path.join(logs_dir,'tensor_board')
    if(not os.path.exists(tb_dir)):
        os.makedirs(tb_dir)
    
    (trainset_iter_lr,trainset_iter_hr,valset_iter)=dataset

    with tf.Session(config=tf.ConfigProto(log_device_placement=device_verbose)) as sess:
        sess.run(tf.global_variables_initializer())
        writer=tf.summary.FileWriter(tb_dir, sess.graph)

        checkpoints=[f for f in os.listdir(logs_dir) if f.startswith('checkpoint_')]
        if(continue_training and len(checkpoints)>0 ):
            last_checkpoint= max(checkpoints)
            start_ep=int(last_checkpoint.split('_')[1])+1
            last_checkpoint = os.path.join(logs_dir,last_checkpoint,'checkpoint.ckpt')
            model.saver.restore(sess,last_checkpoint)
            print('Checkpoint restored from: ',last_checkpoint)
            print('Restarting training from epoch {}'.format(start_ep))
            #lr=start_lr/(10**int(start_ep/decay_steps))
        else:
            start_ep=0

        if(os.path.exists(os.path.join(logs_dir,'record'))):
            max_acc=np.max([float(d) for d in os.listdir(os.path.join(logs_dir,'record'))])/100
            print('record up to now: ',max_acc)
        else:
            max_acc=0.0
            print('record up to now: ',max_acc)
        
        res=[]

        for ep in range(start_ep,num_epochs):           
            start=time.time()   
            print('Epoch {}/{}'.format(ep+1,num_epochs))
            if(not end_lr is None):
                lr=(1-ep/num_epochs)*start_lr + end_lr*(ep/num_epochs)
                
            print('learning rate: ',lr)
            print('memory before epoch: {}GB'.format(psutil.virtual_memory().used / 2 ** 30))
            epoch_loss,epoch_accuracy,_=run_epoch_sc(sess,trainset_iter_lr,trainset_iter_hr,model,training=True,lr=lr,max_steps=max_epoch_steps[0],verbose=verbose,drp_fc1=drp_fc1,drp_fc2=drp_fc2,drp_conv=drp_conv)
            print('memory after epoch: {}GB'.format(psutil.virtual_memory().used / 2 ** 30))
            
            print('duration: ',time.time()-start)
            
            add_summary(writer,'train_loss_LR',epoch_loss[1],ep)
            add_summary(writer,'train_loss_HR',epoch_loss[0],ep)
            add_summary(writer,'train_accuracy_LR',epoch_accuracy[1],ep)
            add_summary(writer,'train_accuracy_HR',epoch_accuracy[0],ep)

            if((ep+1)%save_freq==0 or ep==num_epochs-1):
                checkpoint=model.saver.save(sess,os.path.join(logs_dir,'checkpoint_{}/checkpoint.ckpt'.format(ep)))
                if(os.path.exists(os.path.join(logs_dir,'checkpoint_{}'.format(ep-save_freq)))):
                    print(os.path.join(logs_dir,'checkpoint_{}'.format(ep-save_freq)))
                    shutil.rmtree(os.path.join(logs_dir,'checkpoint_{}'.format(ep-save_freq)))

                
                print('checkpoint saved at: ',checkpoint)
                writer.close()
                writer.reopen()

            if((ep+1)%eval_freq==0 or ep>=num_epochs-10):
                print('-'*50)
                print('Evaluation')
                start=time.time()
                test_loss,test_accuracy,_=run_epoch_sc(sess,valset_iter,None,model,training=False,max_steps=max_epoch_steps[1],verbose=verbose,drp_fc1=0.0,drp_fc2=0.0,drp_conv=0.0)
                add_summary(writer,'validation_loss_LR',test_loss[1],ep/eval_freq)
                add_summary(writer,'validation_accuracy_LR',test_accuracy[1],ep/eval_freq)
                print('duration: ',time.time()-start)
                if(test_accuracy[1]>max_acc):
                    if(os.path.exists(os.path.join(logs_dir,'record'))):
                        shutil.rmtree(os.path.join(logs_dir,'record'))

                    max_acc=test_accuracy[1]
                    dir=os.path.join(logs_dir,'record/{}'.format(str(max_acc*100)))
                    os.makedirs(dir)
                    checkpoint=model.saver.save(sess,os.path.join(dir,'checkpoint.ckpt'))
                    print('record checkpoint saved at ',checkpoint)
                res.append([test_loss,test_accuracy])
                print('-'*70)
            
            print('-'*70)
            
        writer.close()  
        print('-'*100)
        print('Final evaluation on testset')
        test_loss,test_accuracy,report=run_epoch_sc(sess,valset_iter,None,model,training=False,max_steps=max_epoch_steps[1],overlap=0,verbose=verbose,drp_fc1=0.0,drp_fc2=0.0,drp_conv=0.0)
        save_results(report,logs_dir)
        print(' test loss: ',test_loss)
        print(' test accuracy: ',test_accuracy)
        res=np.array(res)
        print(' mean on last 10 step: ',np.mean(res[-10:],axis=0))

def record_folders(dataset,dataset_dir,res,training=True,group_out='g25'):
    if(dataset == 'hmdb51'):
        if(training):
            return  [os.path.join(dataset_dir,'{}_256'.format(res),'train')]
        else:
            return  [os.path.join(dataset_dir,'{}_256'.format(res),'test')]+[os.path.join(dataset_dir,'{}_256'.format(res),'validation')]
    elif(dataset == 'ixmas'):
        dir=os.path.join(dataset_dir,'{}_INTER_POLAR'.format(res))
        if(training):
            return [os.path.join(dir,f)  for f in os.listdir(dir) if not f.startswith(group_out)]
        else:
            return [os.path.join(dir,f)  for f in os.listdir(dir) if f.startswith(group_out)]
    elif(dataset == 'ucf50'):
        dir=os.path.join(dataset_dir,'{}_256_PNG'.format(res))
        if(training):
            return [os.path.join(dir,f)  for f in os.listdir(dir) if f!=group_out]
        else:
            return [os.path.join(dir,f)  for f in os.listdir(dir) if f==group_out]
    elif(dataset=='minikinetics_200'):
        if(training):
            return  [os.path.join(dataset_dir,res,'train_{}'.format(i+1)) for i in range(4)]
        else:
            return  [os.path.join(dataset_dir,res,'validation')]