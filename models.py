from layers import *
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()

def semi_coupled_c3d(inputs,coupling_ratios,training,res,drp,mean_removal=False,noise=False,div_std=False,fs=5,fd=5,k=1):
    
    print('--'*50)
    if(mean_removal==True):
        std=tf.math.reduce_std(inputs)
        avg=tf.reduce_mean(inputs,axis=1,keepdims=True)
        inputs=(inputs-avg)
        
        if(div_std == True):
            inputs=inputs/std

        
    if(noise==True and not training is None):
        inputs=tf.cond(tf.equal(training,True),
                        true_fn=lambda: inputs + tf.random_normal(shape=tf.shape(inputs),mean=0,stddev=0.05),
                        false_fn=lambda: inputs,
                        name='add_noise')

    features={}
    padding='SAME'

    #conv1
    conv1 = ps_conv3d(inputs,filter_depth=fd, filter_size=fs,num_filters=k*32,coupling_ratio=coupling_ratios[0],strides=[1,1,1,1,1],
                  name='conv1',res=res, padding=padding,training=training,use_bias=False, activation=tf.nn.relu)
    conv1 = tf.nn.max_pool3d(input=conv1, ksize=[2, 2, 2], strides=[2, 2, 2], padding=padding)
    conv1=tf.nn.dropout(conv1,rate=drp)
    #print(conv1)
    #conv2 
    conv2 = ps_conv3d(conv1,filter_depth=fd, filter_size=fs,num_filters=k*64,coupling_ratio=coupling_ratios[1],strides=[1,1,1,1,1],
                  name='conv2',res=res, padding=padding,training=training,use_bias=False, activation=tf.nn.relu)
    conv2 = tf.nn.max_pool3d(input=conv2, ksize=[2, 2, 2], strides=[2, 2, 2], padding=padding)
    conv2=tf.nn.dropout(conv2,rate=drp)
  
    #conv3
    conv3 = ps_conv3d(conv2,filter_depth=fd, filter_size=fs,num_filters=k*128,coupling_ratio=coupling_ratios[2],strides=[1,1,1,1,1],
                  name='conv3',res=res, padding=padding,training=training,use_bias=False, activation=tf.nn.relu)
    conv3 = tf.nn.max_pool3d(input=conv3, ksize=[2, 2, 2], strides=[2, 2, 2], padding=padding)
  
    features['conv1']=conv1
    features['conv2']=conv2
    features['conv3']=conv3
   
    return conv3,features

def semi_coupled_c3d_spade(inputs,segmap,coupling_ratios,training,res,k=1):
    features={}
    padding='SAME'
    
    #conv1 
    conv1 = ps_conv3d(inputs,filter_depth=5, filter_size=5,num_filters=k*32,coupling_ratio=coupling_ratios[0],strides=[1,1,1,1,1],
                  name='conv1_space',res=res, padding=padding,training=training,use_bias=False, activation=tf.nn.relu)
    
    conv1 = spade_norm(conv1,segmap,training,name='spade1_{}'.format(res))
    conv1 = tf.nn.max_pool3d(input=conv1, ksize=[2, 2, 2], strides=[2, 2, 2], padding=padding)
    
    #conv2
    conv2 = ps_conv3d(conv1,filter_depth=5, filter_size=5,num_filters=k*64,coupling_ratio=coupling_ratios[1],strides=[1,1,1,1,1],
                  name='conv2_space',res=res, padding=padding,training=training,use_bias=False, activation=tf.nn.relu)

    conv2 = spade_norm(conv2,segmap,training,name='spade2_{}'.format(res))
    conv2 = tf.nn.max_pool3d(input=conv2, ksize=[2, 2, 2], strides=[2, 2, 2], padding=padding)
   
    #conv3
    conv3 = ps_conv3d(conv2,filter_depth=5, filter_size=5,num_filters=k*128,coupling_ratio=coupling_ratios[2],strides=[1,1,1,1,1],
                  name='conv3_space',res=res, padding=padding,training=training,use_bias=False, activation=tf.nn.relu)
    
    conv3 = spade_norm(conv3,segmap,training,name='spade3_{}'.format(res))
    conv3 = tf.nn.max_pool3d(input=conv3, ksize=[2, 2, 2], strides=[2, 2, 2], padding=padding)

    features['conv1']=conv1
    features['conv2']=conv2
    features['conv3']=conv3
    
    return conv3,features

def semi_coupled_c3d_multiscale_sum(inputs,coupling_ratios,training,res,drp,mean_rem,k=1,fs=5,fd=5):
    features={}
    padding='SAME'
    mode='CONSTANT'
    conv1,conv2,conv3=[],[],[]

    for i,stream in enumerate(inputs):
        if(mean_rem[i]):
            stream=stream-tf.reduce_mean(stream,axis=1,keepdims=True)

        x = ps_conv3d(stream,filter_depth=fd, filter_size=fs,num_filters=k*32,coupling_ratio=coupling_ratios[0],strides=[1,1,1,1,1],
                  name='stream{}_conv1'.format(i),res=res, padding=padding,training=training,use_bias=False, activation=tf.nn.relu)

        x = tf.nn.max_pool3d(input=x, ksize=[2, 2, 2], strides=[2, 2, 2], padding=padding)
        conv1.append(x)

    fused=ps_conv3d(tf.add_n(conv1),filter_depth=fd, filter_size=fs,num_filters=k*64,coupling_ratio=coupling_ratios[0],strides=[1,1,1,1,1],
                  name='stream{}_conv_fused_1'.format(i),res=res, padding=padding,training=training,use_bias=False, activation=tf.nn.relu)
    fused= tf.nn.max_pool3d(input=fused, ksize=[2, 2, 2], strides=[2, 2, 2], padding=padding)
    
    for i,stream in enumerate(conv1):
        x = ps_conv3d(stream,filter_depth=fd, filter_size=fs,num_filters=k*64,coupling_ratio=coupling_ratios[1],strides=[1,1,1,1,1],
                  name='stream{}_conv2'.format(i),res=res, padding=padding,training=training,use_bias=False, activation=tf.nn.relu)
        
        x = tf.nn.max_pool3d(input=x, ksize=[2, 2, 2], strides=[2, 2, 2], padding=padding)
        conv2.append(x)

    conv2.append(fused)
    
    fused2 = ps_conv3d(tf.add_n(conv2),filter_depth=fd, filter_size=fs,num_filters=k*128,coupling_ratio=coupling_ratios[1],strides=[1,1,1,1,1],
                  name='stream{}_conv_fused_2'.format(i),res=res, padding=padding,training=training,use_bias=False, activation=tf.nn.relu)
    fused2 = tf.nn.max_pool3d(input=fused2, ksize=[2, 2, 2], strides=[2, 2, 2], padding=padding)
    
    for i,stream in enumerate(conv2):
        x = ps_conv3d(stream,filter_depth=fd, filter_size=fs,num_filters=k*128,coupling_ratio=coupling_ratios[2],strides=[1,1,1,1,1],
                    name='stream{}_conv3'.format(i),res=res, padding=padding,training=training,use_bias=False, activation=tf.nn.relu)
   
        x = tf.nn.max_pool3d(input=x, ksize=[2, 2, 2], strides=[2, 2, 2], padding=padding)
        conv3.append(x)
   
    conv3.append(fused2)
    
    fused3 = tf.add_n(conv3)
    #fused3 = ps_conv3d(tf.add_n(conv3),filter_depth=fd, filter_size=fs,num_filters=128,coupling_ratio=coupling_ratios[2],strides=[1,1,1,1,1],
    #              name='stream{}_conv_fused_3'.format(i),res=res, padding=padding,training=training,use_bias=False, activation=tf.nn.relu)
    
    return fused3,features