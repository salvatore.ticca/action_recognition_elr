from models import *
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()

class MSN:
    def __init__(self,config):

        input_shape=[None,config.INPUT_FRAMES,config.HR_RES[1],config.HR_RES[0],3]
        self.rgb = tf.placeholder(tf.float32, shape=input_shape, name='input_rgb')
        input_shape[-1]=2
        self.flow = tf.placeholder(tf.float32, shape=input_shape, name='input_flow')
        input_shape[-1]=1
        self.fg_mask = tf.placeholder(tf.float32, shape=input_shape, name='input_fg_mask')
        self.fine_labels = tf.placeholder(tf.int64, shape=[None], name='input_labels_fine')
        self.global_step=tf.Variable(0, trainable=False, name='global_step')
        self.training = tf.placeholder(tf.bool, name="is_training")
        self.dropout_fc1 = tf.placeholder(tf.float32, shape=(), name='dropout_fc1')
        self.dropout_fc2 = tf.placeholder(tf.float32, shape=(), name='dropout_fc2')
        self.dropout_conv = tf.placeholder(tf.float32, shape=(), name='dropout_conv')
        self.learning_rate = tf.placeholder(tf.float32, shape=(), name='learning_rate')
 
        assert(config.RGB_STREAM or config.OPTICAL_FLOW_STREAM or config.MASK_STREAM)
        self.rgb_stream=config.RGB_STREAM
        self.optical_flow_stream=config.OPTICAL_FLOW_STREAM
        self.mask_stream=config.MASK_STREAM
        
        logits_lr=self.__compute_logits('LR',config)
        logits_hr=self.__compute_logits('HR',config)

        with tf.variable_scope('weigth_decay'):
            lr_weights=[tf.square(tf.nn.l2_loss(var)) for var in tf.get_collection(tf.GraphKeys.WEIGHTS) if 'LR' in var.name or 'shared' in var.name]
            hr_weights=[tf.square(tf.nn.l2_loss(var)) for var in tf.get_collection(tf.GraphKeys.WEIGHTS) if 'HR' in var.name or 'shared' in var.name]
           
            lr_decay=config.L2*tf.add_n(lr_weights)
            hr_decay=config.L2*tf.add_n(hr_weights)
           
        with tf.variable_scope('loss'):
            self.loss_lr = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits_lr,labels=self.fine_labels)) 
            self.loss_hr = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits_hr,labels=self.fine_labels)) 

            self.loss_lr_opt = self.loss_lr + lr_decay 
            self.loss_hr_opt = self.loss_hr + hr_decay 
        
        with tf.variable_scope('accuracy'):
            self.predictions_lr=tf.argmax(logits_lr,axis=1,output_type=tf.int64)
            self.accuracy_lr=tf.reduce_mean(tf.cast(tf.equal(self.predictions_lr,self.fine_labels),tf.float32))

            self.predictions_hr=tf.argmax(logits_hr,axis=1,output_type=tf.int64)
            self.accuracy_hr=tf.reduce_mean(tf.cast(tf.equal(self.predictions_hr,self.fine_labels),tf.float32))

            self.predictions_eval=tf.argmax(tf.reduce_mean(logits_lr,axis=0),axis=0,output_type=tf.int32)

        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            if(config.OPTIMIZER=='rmsprop'):
                self.train_op_hr=tf.train.RMSPropOptimizer(self.learning_rate,momentum=0.3).minimize(self.loss_hr_opt)
                self.train_op_lr=tf.train.RMSPropOptimizer(self.learning_rate,momentum=0.3).minimize(self.loss_lr_opt,self.global_step)
            elif(config.OPTIMIZER=='momentum'):
                self.train_op_hr=tf.train.MomentumOptimizer(self.learning_rate,momentum=0.9).minimize(self.loss_hr_opt)
                self.train_op_lr=tf.train.MomentumOptimizer(self.learning_rate,momentum=0.9).minimize(self.loss_lr_opt,self.global_step)
            else:
                self.train_op_hr=tf.train.AdamOptimizer(self.learning_rate).minimize(self.loss_hr_opt)
                self.train_op_lr=tf.train.AdamOptimizer(self.learning_rate).minimize(self.loss_lr_opt,self.global_step)
        
        self.saver=tf.train.Saver()
    
    def __compute_logits(self,res,config):
        features=[]
        hidden=[]
        if(config.FUSION=='spade'):
            assert(config.RGB_STREAM and config.OPTICAL_FLOW_STREAM and config.MASK_STREAM)
            with tf.variable_scope('rgb_stream',reuse=tf.AUTO_REUSE):
                x,h=semi_coupled_c3d_spade(self.rgb,self.fg_mask,config.COUPLING_RATIOS,self.training,res,k=config.K)
                features.append(x)

            with tf.variable_scope('optical_flow_stream',reuse=tf.AUTO_REUSE):
                x,h=semi_coupled_c3d_spade(self.flow,self.fg_mask,config.COUPLING_RATIOS,self.training,res,k=config.K)
                features.append(x)

        elif(config.FUSION=='multiscale_sum'):
            with tf.variable_scope('features_computation',reuse=tf.AUTO_REUSE):
                x,_=semi_coupled_c3d_multiscale_sum(inputs=[self.rgb,self.flow,self.fg_mask],res=res,coupling_ratios=config.COUPLING_RATIOS,
                                                    training=self.training,drp=self.dropout_conv,mean_rem=[True,False,True],k=config.K)
        else:
            if(self.rgb_stream==True):
                with tf.variable_scope('rgb_stream',reuse=tf.AUTO_REUSE):
                    out,h=semi_coupled_c3d(self.rgb,config.COUPLING_RATIOS,self.training,res,
                                           self.dropout_conv,mean_removal=True,div_std=False,fs=config.FILTER_SIZE,fd=config.FILTER_DEPTH,k=config.K)
                    features.append(out)
                    hidden.append(h)
                    
            if(self.mask_stream==True):  
                with tf.variable_scope('mask_stream',reuse=tf.AUTO_REUSE):
                    out,h=semi_coupled_c3d(self.fg_mask,config.COUPLING_RATIOS,self.training,res,
                                           self.dropout_conv,mean_removal=True,div_std=False,fs=config.FILTER_SIZE,fd=config.FILTER_DEPTH,k=config.K)
                    features.append(out)
                    hidden.append(h)
                    
            if(self.optical_flow_stream==True):
                with tf.variable_scope('optical_flow_stream',reuse=tf.AUTO_REUSE):
                    out,h=semi_coupled_c3d(self.flow,config.COUPLING_RATIOS,self.training,res,
                                           self.dropout_conv,mean_removal=False,fs=config.FILTER_SIZE,fd=config.FILTER_DEPTH,k=config.K)
                    features.append(out)      
                    hidden.append(h)

        with tf.variable_scope('stream_fusion',reuse=tf.AUTO_REUSE):
            if(config.FUSION=='sum' or config.FUSION=='spade'):
                x=self.sum_fusion(features)
            elif(config.FUSION=='concat'):
                x=self.concat_fusion(features)
            elif(config.FUSION=='inter'):                
                x=self.interpolation_fusion(features,res,config.COUPLING_RATIOS[2])                
            elif(config.FUSION=='ssma'):
                x=self.ssma_fusion(features,res,config.COUPLING_RATIOS[2])
            elif(config.FUSION=='multiscale_ssma' or config.FUSION=='multiscale_sum'):
                pass
                #no further fusion needed

        if(config.FUSION=='late'):
            for i in range(len(features)):
                x=tf.nn.max_pool3d(input=features[i], ksize=[4, 1, 1], strides=[4, 1, 1], padding='SAME')
                
                features[i]=self.fully_connected(x,config.NUM_CLASSES,res,config.COUPLING_RATIOS,'fully_connected_stream{}'.format(i),k=config.K)
                
            features=tf.stack(features,axis=1)            
            return tf.reduce_mean(features,axis=1)
        else:
            x=tf.nn.max_pool3d(input=x, ksize=[4, 1, 1], strides=[4, 1, 1], padding='SAME')
            out=self.fully_connected(x,config.NUM_CLASSES,res,config.COUPLING_RATIOS,'fully_connected',k=config.K)
            return out

        
    def fully_connected(self,x,num_classes,res,coupling_ratios,name,k=1):
        with tf.variable_scope(name,reuse=tf.AUTO_REUSE):
            x=tf.nn.dropout(x,rate=self.dropout_fc1)
            x = ps_dense(x,output_units=k*256,coupling_ratio=coupling_ratios[3],name='fc1',res=res,use_bias=True,training=None,activation=tf.nn.relu)

            x=tf.nn.dropout(x,rate=self.dropout_fc2)   
            x = ps_dense(x,output_units=k*512,coupling_ratio=coupling_ratios[4],name='fc2',res=res,use_bias=True,training=None,activation=tf.nn.relu)
            
            return ps_dense(x,output_units=num_classes,coupling_ratio=coupling_ratios[5],name='fc_logits',res=res,use_bias=True,training=None,activation=None)


    def concat_fusion(self,features):
        return tf.concat(features,axis=-1)

    def sum_fusion(self,features):
        return tf.add_n(features)

    def interpolation_fusion(self,features,res,coupling_ratio):
        with tf.variable_scope('interpolation',reuse=tf.AUTO_REUSE):
            ch=features[0].shape[-1].value
            print('-'*25)

            stream_indep=ps_conv3d(tf.concat(features,axis=-1),filter_depth=3,filter_size=1,num_filters=ch,strides=[1,1,1,1,1],
                                   res=res,coupling_ratio=coupling_ratio,padding='SAME',name='conv_indep',use_bias=False,training=None,activation=None)
            out=0.0
            for i in range(len(features)):
                alpha=ps_conv3d(tf.concat([features[i],stream_indep],axis=-1),filter_depth=3,filter_size=1,
                                num_filters=ch,strides=[1,1,1,1,1],res=res,coupling_ratio=coupling_ratio,
                                padding='SAME',name='conv_{}'.format(i),use_bias=False,training=None,activation=tf.nn.sigmoid)
                
                out=out+features[i]*alpha
            return out

    def ssma_fusion(self,features,res,coupling_ratio):
        with tf.variable_scope('ssma',reuse=tf.AUTO_REUSE):
            ch=128
            inputs=tf.concat(features,axis=-1)
            
            x=ps_conv3d(inputs,filter_depth=1,filter_size=1,num_filters=len(features)*ch,strides=[1,1,1,1,1],res=res,coupling_ratio=coupling_ratio
                     ,padding='SAME',name='conv2',use_bias=False,training=None,activation=tf.nn.sigmoid)
            
            x=inputs*x
            return x