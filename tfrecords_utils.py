import os
import numpy as np
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()

def idx_frame_replication(num_frames,input_frames,randomize):
    idx=tf.linspace(0.0,num_frames-1.0,num=input_frames)
    start=tf.random.randint(minval=0,maxval=tf.min(num_frames-input_frames,0))
    if(randomize):
        maxval=num_frames/input_frames
        rnd=tf.pad(tf.random.uniform([input_frames-2],minval=0.0,maxval=maxval,dtype=tf.float32),[[1,1]])
        idx=idx+rnd
    return tf.cast(idx,tf.int64)

def idx_videoloop(num_frames,input_frames,randomize):
    if(randomize):
        maxval=tf.math.maximum(0.0,num_frames-input_frames)
        start=tf.random.uniform(minval=0,maxval=maxval,shape=(),dtype=tf.float32)
    else:
        start=0.0

    idx=tf.linspace(start,start+input_frames-1,num=input_frames)
    return tf.cast(idx,tf.int64)

def idx_videoloop_separated(num_frames,input_frames,randomize,separation=8):
    if(randomize):
        x=tf.math.maximum(0.0,num_frames-input_frames)
        maxval=tf.cast(tf.math.ceil(x/separation),tf.int32)
        start=tf.math.minimum(tf.cast(x,tf.int32),
                              tf.random.uniform(minval=0,maxval=maxval+1,shape=(),dtype=tf.int32)*separation)
    else:
        start=0
    
    return tf.range(start,start+input_frames)
    
def parse_tfexample(example,input_frames,flip_prob=0.5,randomize=False,crop_size=None,flow_bound=40.0):
    feature_description = {
        'frames':  tf.io.VarLenFeature(tf.string),
        #'fg_mask':  tf.io.VarLenFeature(tf.string),
        'fg_mask': tf.io.FixedLenSequenceFeature([], tf.float32,allow_missing=True),
        'fg_mask_shape': tf.io.FixedLenSequenceFeature([], tf.int64,allow_missing=True) ,
        'flow':tf.io.FixedLenSequenceFeature([], tf.float32,allow_missing=True),
        'flow_shape': tf.io.FixedLenSequenceFeature([], tf.int64,allow_missing=True) ,
        'shape': tf.io.FixedLenSequenceFeature([], tf.int64,allow_missing=True), 
        'video_name': tf.io.FixedLenFeature([], tf.string),
        'action_label': tf.io.FixedLenFeature([], tf.string),
        'action_id': tf.io.FixedLenFeature([], tf.int64)          
    }

    parsed_example = tf.io.parse_single_example(example, feature_description)

    video_name=tf.cast(parsed_example['video_name'],tf.string)
    action_label=tf.cast(parsed_example['action_label'],tf.string)
    action_id=tf.cast(parsed_example['action_id'],tf.int64)
    shape=tf.cast(parsed_example['shape'],tf.int64)
    num_frames=tf.cast(shape[0],tf.float32)
    rnd=tf.random.uniform((),dtype=tf.float32)
    rnd_hr=tf.random.uniform((),dtype=tf.float32)

    #PARSE FOREGROUND MASK
    #fg_mask=tf.map_fn(lambda i: tf.cast(tf.io.decode_png(parsed_example['fg_mask'].values[i]),tf.float32)/255.0,
    #                   tf.cast(tf.linspace(0.0,num_frames-1.0,num=tf.cast(num_frames,tf.int64)),tf.int64),dtype=tf.float32)
    
    fg_mask_shape=tf.cast(parsed_example['fg_mask_shape'],tf.int64)
    fg_mask=tf.reshape(tf.cast(parsed_example['fg_mask'],tf.float32),fg_mask_shape)
    fg_mask=tf.cond(rnd>=flip_prob,lambda: fg_mask,lambda: tf.image.flip_left_right(fg_mask))

    #PARSE OPTICAL FLOW
    flow_shape=tf.cast(parsed_example['flow_shape'],tf.int64)
    flow=tf.reshape(tf.cast(parsed_example['flow'],tf.float32),flow_shape)
    flow=flow[:,:,:,0:2]
    flow=(flow/127.5)-1
    
    flow=tf.cond(rnd>=flip_prob,lambda: flow,lambda: tf.image.flip_left_right(flow))

    #PARSE FRAMES
    frames=tf.map_fn(lambda i: tf.cast(tf.io.decode_png(parsed_example['frames'].values[i]),tf.float32)/255.0,
                        tf.cast(tf.linspace(0.0,num_frames-1.0,num=tf.cast(num_frames,tf.int64)),tf.int64),dtype=tf.float32) 
    
    frames=tf.cond(rnd>=flip_prob,lambda: frames,lambda: tf.image.flip_left_right(frames))
    
    n=1
    idx=0
    if(input_frames > 0 ):
        n=tf.cast(1 + input_frames/num_frames,tf.int32)
        idx=idx_videoloop_separated(num_frames,input_frames,randomize,16)
        
        fg_mask=tf.tile(fg_mask,[n,1,1,1])
        fg_mask=tf.gather(fg_mask,indices=idx,axis=0)   
       
        flow=tf.tile(flow,[n,1,1,1])
        flow=tf.gather(flow,indices=idx,axis=0) 
       
        frames=tf.tile(frames,[n,1,1,1])
        frames=tf.gather(frames,indices=idx,axis=0)  

    #RANDOM CROP
    if(not crop_size is None):
        concat=tf.concat([frames,flow,fg_mask],axis=-1)
        crop=tf.image.random_crop(concat,[tf.shape(frames)[0],crop_size,crop_size,6])
        crop=tf.image.resize(crop,[tf.shape(frames)[1],tf.shape(frames)[2]])
        frames=crop[:,:,:,0:3]

    return (frames,flow,fg_mask,action_id,action_label,video_name)

def filter_classes(a, classes):
    class2keep=tf.equal(classes, a)
    return tf.reduce_any(class2keep)

def reindex_classes(a,classes):
    i=tf.where(tf.equal(classes, a))
    return tf.cast(tf.squeeze(i),tf.int64)

def prepare_dataset(tfrecords_dirs,input_frames,shuffle_buffer=None,repeat=False,batch_size=None,flip_prob=0.5,randomize=False,crop_size=None,keep_classes=None):
    tfrecords=[]
    for d in tfrecords_dirs:
        tfrecords+=[os.path.join(d,rec) for rec in os.listdir(d) if rec.endswith('.tfrecords')]
    
    print(tfrecords)
    
    dataset = tf.data.Dataset.from_tensor_slices(tfrecords).interleave(lambda x:
               tf.data.TFRecordDataset(x),
               cycle_length=len(tfrecords),
                block_length=1,
                num_parallel_calls=tf.data.experimental.AUTOTUNE).map(lambda x: 
                                                                      parse_tfexample(x,input_frames=input_frames,flip_prob=flip_prob,
                                                                                                randomize=randomize,crop_size=crop_size),num_parallel_calls=tf.data.experimental.AUTOTUNE)
                
    #dataset=tf.data.TFRecordDataset(tfrecords).map(lambda x: parse_tfexample(x,input_frames=input_frames,flip_prob=flip_prob,randomize=randomize,
    #                                                                            parse_frames=parse_frames,parse_flow=parse_flow),
    #                                               num_parallel_calls=tf.data.experimental.AUTOTUNE)

    if(not keep_classes is None):
        #filter out unwanted sample
        dataset=dataset.filter(lambda frames,flow,fg_mask,action_id,action_label,video_name: filter_classes(action_id,keep_classes))
        #reindex the classes between 0 and config.KEEP_CLASSES
        dataset=dataset.map(lambda frames,flow,fg_mask,action_id,action_label,video_name: (frames,
                                                                                            flow,
                                                                                            fg_mask,
                                                                                            reindex_classes(action_id,keep_classes),
                                                                                            action_label,
                                                                                            video_name) )
    if(not shuffle_buffer is None and shuffle_buffer>0):
        dataset=dataset.shuffle(shuffle_buffer)
        print('SHUFFLE')

    if(repeat):
        dataset=dataset.repeat()
        print('REPEAT')

    #dataset=dataset.map(lambda x: parse_tfexample(x,input_frames=input_frames,flip_prob=flip_prob,randomize=randomize),num_parallel_calls=tf.data.experimental.AUTOTUNE)
    
    if(not batch_size is None and batch_size>0):
        dataset=dataset.batch(batch_size,drop_remainder=False)

    return dataset.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)