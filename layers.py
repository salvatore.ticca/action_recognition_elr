import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()

def get_weights(shape,initializer,name):
    return tf.get_variable(name=name,shape=shape,initializer=initializer,
                                    collections=[tf.GraphKeys.WEIGHTS,tf.GraphKeys.GLOBAL_VARIABLES])
    
def get_psweights(shape,initializer,coupling_ratio,res,name='weights'):
    num_filters=shape[-1]
    shared_filters=int(coupling_ratio * num_filters)
    res_specific_filters=num_filters-shared_filters

    weights=[]
    if(res_specific_filters != 0):
        shape[-1]=res_specific_filters
        res_specific_weights=tf.get_variable(name='{}_{}'.format(res,name),shape=shape,initializer=initializer,
                                     collections=[tf.GraphKeys.WEIGHTS,tf.GraphKeys.GLOBAL_VARIABLES])

        weights.append(res_specific_weights)

    
    if(shared_filters != 0):
        shape[-1]=shared_filters
        shared_weights=tf.get_variable(name='shared_{}'.format(name),shape=shape,initializer=initializer,
                                     collections=[tf.GraphKeys.WEIGHTS,tf.GraphKeys.GLOBAL_VARIABLES])

        weights.append(shared_weights)
    
    print(weights)

    return tf.concat(weights,axis=-1)

def conv2p1d(inputs,filter_depth,filter_size,num_filters,strides,name,padding='SAME',training=None,activation=None,use_bias=False):
    with tf.variable_scope(name):
        input_channels=inputs.shape[-1]
        Mi=int((filter_depth*filter_size*filter_size*input_channels.value*num_filters)/
                        (filter_size*filter_size*input_channels.value+filter_depth*num_filters))

        spatial_strides=[1,1]+strides[2:]

        spatial_conv=conv3d(inputs,
                            filter_depth=1,
                            filter_size=filter_size,
                            num_filters=Mi,
                            strides=spatial_strides,
                            padding=padding,
                            training=None,
                            activation=activation,
                            use_bias=use_bias,
                            name='2d_conv')

        temporal_strides=[1,strides[1],1,1,1]

        temporal_conv=conv3d(spatial_conv,
                             filter_depth=filter_depth,
                             filter_size=1,
                             num_filters=num_filters,
                             strides=temporal_strides,
                             padding=padding,
                             training=training,
                             activation=activation,
                             use_bias=use_bias,
                             name='1d_conv')  
            
        return temporal_conv 

def conv3d(inputs,filter_depth,filter_size,num_filters,strides,name,padding='SAME',training=None,activation=None,use_bias=False):
    with tf.variable_scope(name):
        input_channels=inputs.shape[-1]
            
        weights = get_weights(name='weights', 
                                    shape=[filter_depth,filter_size,filter_size,input_channels,num_filters],
                                    initializer=tf.truncated_normal_initializer(stddev=1e-3))
        conv=tf.nn.conv3d(input=inputs, filters=weights,strides=strides, padding=padding) 
        if(use_bias):
            bias = get_weights(name='bias',shape=[num_filters],initializer=tf.truncated_normal_initializer(stddev=1e-3))
            conv=conv+bias

        if(not activation is None):
            conv=activation(conv)
        
        if(not training is None):
            conv = batch_norm(conv,training,'batch_norm')
      
        return conv

def dense(inputs,output_units,name,training=None,use_bias=False,activation=None,flatten=True):
    with tf.variable_scope(name):
        if(flatten==True):
            inputs=tf.layers.flatten(inputs)

        weights=get_weights(name='weights', 
                                shape=[inputs.shape[-1],output_units],
                                initializer=tf.truncated_normal_initializer(stddev=0.02))
           
        dense=tf.matmul(inputs,weights)
        
        if(use_bias):
            bias=get_weights(name='bias',shape=[output_units],initializer=tf.truncated_normal_initializer(stddev=0.02))
            dense=dense+bias

        if(not activation is None):
            dense=activation(dense)
        
        if(not training is None):
            dense = batch_norm(dense,training,'batch_norm')

        return dense

def batch_norm(x,training,name,scale=True,center=True):
    return tf.layers.batch_normalization(x, training=training,scale=scale,center=center,axis=-1,name=name)

def spade_norm(x,segmap,training,name):
    out_h,out_w,out_c=x.shape[2].value,x.shape[3].value,x.shape[4].value
    
    with tf.variable_scope(name):
        st=int(segmap.shape[1].value/x.shape[1].value)
        sp=int(segmap.shape[2].value/x.shape[2].value)
        
        s=conv3d(segmap,filter_depth=3,filter_size=3,num_filters=out_c,strides=[1,1,1,1,1],padding='SAME',name='conv_segmap')
        s=tf.nn.avg_pool3d(input=segmap, ksize=[st, sp, sp], strides=[st, sp, sp], padding='VALID')
       
        gamma= conv3d(s,filter_depth=3,filter_size=3,num_filters=out_c,strides=[1,1,1,1,1],padding='SAME',name='conv_gamma')
        beta = conv3d(s,filter_depth=3,filter_size=3,num_filters=out_c,strides=[1,1,1,1,1],padding='SAME',name='conv_beta')
        
        normalized=batch_norm(x,training,name='bn',scale=False,center=False)
        return tf.multiply(normalized , (1 + gamma)) + beta

def conv2d(inputs,filter_size,num_filters,strides,name,padding='SAME',use_bias=False,training=None,activation=None):
    with tf.variable_scope(name):
        input_channels=inputs.shape[-1]

        weights = get_weights(name='weights', 
                                    shape=[filter_size,filter_size,input_channels,num_filters],
                                    initializer=tf.truncated_normal_initializer(stddev=1e-3))

        conv=tf.nn.conv2d(input=inputs,filter=weights,strides=strides,padding=padding,name='conv') 
        if(use_bias):
            bias = get_weights(name='bias',shape=[num_filters],initializer=tf.truncated_normal_initializer(stddev=1e-3))
            conv = conv + bias

        if(not training is None):
            conv = batch_norm(conv,training,'batch_norm')

        if(not activation is None):
            conv=activation(conv)

    return conv

def ps_conv2d(inputs,filter_size,num_filters,coupling_ratio,strides,name,res,padding='SAME',training=None,activation=None,use_bias=False):
    with tf.variable_scope(name):
        shared_filters=int(coupling_ratio * num_filters)
        res_specific_filters=num_filters-shared_filters

        features=[]
        if(res_specific_filters != 0):
            features.append(conv2d(inputs,
                          filter_size=filter_size,
                          num_filters=res_specific_filters,
                          strides=strides,
                          name=res,
                          padding=padding,
                          training=None,
                          activation=activation))
        
        if(shared_filters != 0):
            features.append(conv2d(inputs,
                          filter_size=filter_size,
                          num_filters=shared_filters,
                          strides=strides,
                          name='shared',
                          padding=padding,
                          training=None,
                          activation=activation))
            
        out=tf.concat(features,axis=-1)
        
        if(not training is None):
            print('BN CONV')
            out=batch_norm(out,training,'bn_{}'.format(res))
        
        return out

def ps_dense(inputs,output_units,coupling_ratio,name,res,training,use_bias=False,activation=None,flatten=True):
     with tf.variable_scope(name):
        shared_units=int(coupling_ratio * output_units)
        res_specific_units=output_units-shared_units

        features=[]
        if(res_specific_units != 0):
            features.append(dense(inputs,output_units=res_specific_units,name=res,use_bias=use_bias,activation=activation,flatten=flatten))
        
        if(shared_units != 0):
            features.append(dense(inputs,output_units=shared_units,name='shared',use_bias=use_bias,activation=activation,flatten=flatten))

        out=tf.concat(features,axis=-1)
        
        if(not training is None):
            out=batch_norm(out,training,'bn_{}'.format(res))
        
        return out

def ps_conv3d(inputs,filter_depth,filter_size,num_filters,coupling_ratio,strides,name,res,padding='SAME',training=None,activation=None,use_bias=False):
    with tf.variable_scope(name):
        shared_filters=int(coupling_ratio * num_filters)
        res_specific_filters=num_filters-shared_filters

        features=[]
        if(res_specific_filters != 0):
            features.append(conv3d(inputs,
                          filter_depth=filter_depth,
                          filter_size=filter_size,
                          num_filters=res_specific_filters,
                          strides=strides,
                          name=res,
                          padding=padding,
                          training=None,
                          use_bias=use_bias,
                          activation=activation))
        
        if(shared_filters != 0):
            features.append(conv3d(inputs,
                          filter_depth=filter_depth,
                          filter_size=filter_size,
                          num_filters=shared_filters,
                          strides=strides,
                          name='shared',
                          padding=padding,
                          training=None,
                          use_bias=use_bias,
                          activation=activation))
        
        out=tf.concat(features,axis=-1)
        
        if(not training is None):
            out=batch_norm(out,training,'bn_{}'.format(res))
        
        return out

def ps_dense_v1(inputs,output_units,coupling_ratio,name,res,training,use_bias=False,activation=None):
     with tf.variable_scope(name):
        inputs=tf.layers.flatten(inputs)
        weights=get_psweights(name='weights', 
                                shape=[inputs.shape[1],output_units],
                                initializer=tf.truncated_normal_initializer(stddev=0.02),coupling_ratio=coupling_ratio,res=res)

        dense=tf.matmul(inputs,weights)
        
        if(use_bias):
            bias=get_psweights(name='bias',shape=[output_units],initializer=tf.truncated_normal_initializer(stddev=0.02),coupling_ratio=coupling_ratio,res=res)
            dense=dense+bias

        if(not activation is None):
            dense=activation(dense)
        
        if(not training is None):
            dense = batch_norm(dense,training,'batch_norm_{}'.format(res))

        return dense

def ps_conv3d_v1(inputs,filter_depth,filter_size,num_filters,coupling_ratio,strides,name,res,padding='SAME',training=None,activation=None,use_bias=False):
    with tf.variable_scope(name):
        input_channels=inputs.shape[-1]
            
        weights = get_psweights(name='weights',shape=[filter_depth,filter_size,filter_size,input_channels,num_filters],
                                    initializer=tf.truncated_normal_initializer(stddev=1e-3),coupling_ratio=coupling_ratio,res=res)
        conv=tf.nn.conv3d(input=inputs, filters=weights,strides=strides, padding=padding) 
        
        if(use_bias):
            bias = get_psweights(name='bias',shape=[num_filters],initializer=tf.truncated_normal_initializer(stddev=0.02),
                                 coupling_ratio=coupling_ratio,res=res)
            conv=conv+bias

        if(not activation is None):
            conv=activation(conv)
        
        if(not training is None):
            conv = batch_norm(conv,training,'batch_norm_{}'.format(res))
      
        return conv

def ps_conv2p1d_v1(inputs,filter_depth,filter_size,num_filters,coupling_ratio,strides,name,res,padding='SAME',training=None,activation=None,use_bias=False):
    with tf.variable_scope(name):
        shared_filters=int(coupling_ratio * num_filters)
        res_specific_filters=num_filters-shared_filters

        features=[]
        if(res_specific_filters != 0):
            features.append(conv2p1d(inputs,
                          filter_depth=filter_depth,
                          filter_size=filter_size,
                          num_filters=res_specific_filters,
                          strides=strides,
                          name=res,
                          padding=padding,
                          training=None,
                          use_bias=use_bias,
                          activation=activation))
        
        if(shared_filters != 0):
            features.append(conv2p1d(inputs,
                          filter_depth=filter_depth,
                          filter_size=filter_size,
                          num_filters=shared_filters,
                          strides=strides,
                          name='shared',
                          padding=padding,
                          training=None,
                          use_bias=use_bias,
                          activation=activation))
        
        out=tf.concat(features,axis=-1)
        
        if(not training is None):
            out=batch_norm(out,training,'bn')
        
        return out

def ps_conv2p1d_v2(inputs,filter_depth,filter_size,num_filters,coupling_ratio,strides,name,res,padding='SAME',training=None,activation=None,use_bias=False):
    with tf.variable_scope(name):
        input_channels=inputs.shape[-1]
        Mi=num_filters

        spatial_strides=[1,1]+strides[2:]
        spatial_conv=ps_conv3d(inputs,
                            filter_depth=1,
                            filter_size=filter_size,
                            num_filters=Mi,
                            coupling_ratio=coupling_ratio,
                            strides=spatial_strides,
                            padding=padding,
                            training=training,
                            activation=activation,
                            use_bias=use_bias,
                            name='spatial_conv',
                            res=res)
        
        temporal_strides=[1,strides[1],1,1,1]
        temporal_conv=ps_conv3d(spatial_conv,
                             filter_depth=filter_depth,
                             filter_size=1,
                             num_filters=num_filters,
                             coupling_ratio=coupling_ratio,
                             strides=temporal_strides,
                             padding=padding,
                             training=training,
                             activation=activation,
                             use_bias=use_bias,
                             name='temporal_conv',
                             res=res)  
            
        return temporal_conv 