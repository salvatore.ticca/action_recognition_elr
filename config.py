import cv2
import os

interpolation_methods={
        cv2.INTER_NEAREST:'nearest',
        cv2.INTER_CUBIC:'cubic',
        cv2.INTER_LINEAR:'linear',
        -1:'average'
    }

res2str=lambda r: '{}x{}'.format(r[0],r[1])

class Config:
    def __init__(self,dataset,rgb_stream,optical_flow_stream,mask_stream,fusion,batch_size,multiplicator,crop=True,group_out='g02'):
        self.DATASET=dataset
        self.GROUP_OUT=group_out
        self.K=max(1,multiplicator)

        if(self.DATASET=='ixmas'):
            self.LR_RES=[12,16]
            self.DATASET_SIZE=(1620,180)
            self.NUM_CLASSES=12
        elif(self.DATASET=='hmdb51'):
            self.LR_RES=[16,12]
            self.DATASET_SIZE=(3570,1530)
            self.NUM_CLASSES=51
        elif(self.DATASET=='ucf50'):
            self.LR_RES=[16,12]
            self.DATASET_SIZE=(6618-264,264)
            self.NUM_CLASSES=50
        elif(self.DATASET=='minikinetics_200'):
            self.LR_RES=[16,12]
            self.DATASET_SIZE=(34937,2205)
            self.NUM_CLASSES=189

        self.HR_RES=[32,32]# width x height ixmas: [12,16] hmdb:[16,12]
        
        if(crop):
            self.CROP_SIZE=28
        else:
            self.CROP_SIZE=None
            
        self.RANDOMIZE_FRAMES=True
        self.FRAMES_FORMAT='rgb'
        self.INPUT_FRAMES=32
        self.INTERPOLATION=cv2.INTER_CUBIC
        self.BATCH_SIZE=batch_size

        #MODEL PARAMETERS
        self.OPTIMIZER='adam'
        self.L2=1e-4
        
        supported_fusion=['sum','concat','spade','ssma','inter','multiscale_sum']
        if(fusion in supported_fusion):
            self.FUSION=fusion
        else:
            raise Exception('fusion strategy not supported, insert one of the following: {}'.format(supported_fusion))

        self.RGB_STREAM=rgb_stream
        self.OPTICAL_FLOW_STREAM=optical_flow_stream
        self.MASK_STREAM=mask_stream
        self.COUPLING_RATIOS=[1,1,1,1,1,1]
        self.FILTER_SIZE=5
        self.FILTER_DEPTH=5
