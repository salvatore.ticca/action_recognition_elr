import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()
import cv2
import os
import numpy as np
import time
import pickle as pkl
import argparse

def load_dump(path):
    if(os.path.exists(path)):
        obj=pkl.load(open(path,"rb"))
        return obj
    else:
        raise FileNotFoundError('Error, file not found')     


def _bytes_feature(value):
  """Returns a bytes_list from a string / byte."""
  if isinstance(value, type(tf.constant(0))):
    value = value.numpy() # BytesList won't unpack a string from an EagerTensor.
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=value))

def _float_feature(value):
  """Returns a float_list from a float / double."""
  return tf.train.Feature(float_list=tf.train.FloatList(value=value))

def _int64_feature(value):
  """Returns an int64_list from a bool / enum / int / uint."""
  return tf.train.Feature(int64_list=tf.train.Int64List(value=value))

def get_tfexample(frames,action_label,action_id,video_name,flow=None,fg_mask=None):
    encoded_frames=[cv2.imencode(".png", frame)[1].tobytes() for frame in frames]
    features = {
        'frames': _bytes_feature(encoded_frames),#_float_feature(frames.flatten()),
        'video_name':_bytes_feature([video_name.encode()]),
        'action_label':_bytes_feature([action_label.encode()]),
        'action_id': _int64_feature([action_id]),
        'shape':  _int64_feature(list(frames.shape)),
    }

    if(not flow is None):
        features['flow'] = _float_feature(flow.flatten())
        features['flow_shape'] =  _int64_feature(list(flow.shape)),
        #encoded_flow=[cv2.imencode(".png", f)[1].tobytes() for f in flow]
        #features['flow']=_bytes_feature(encoded_flow)
    
    if(not fg_mask is None):
        encoded_mask=[cv2.imencode(".png", f)[1].tobytes() for f in fg_mask]
        features['fg_mask']=_bytes_feature(encoded_mask)

    return tf.train.Example(features=tf.train.Features(feature=features))

def parse_videoinfo(example):
    feature_description = {
        'frames':  tf.io.VarLenFeature(tf.string),
        'fg_mask':  tf.io.VarLenFeature(tf.string),
        'flow':tf.io.FixedLenSequenceFeature([], tf.float32,allow_missing=True),
        'flow_shape': tf.io.FixedLenSequenceFeature([], tf.int64,allow_missing=True) ,
        'shape': tf.io.FixedLenSequenceFeature([], tf.int64,allow_missing=True), 
        'video_name': tf.io.FixedLenFeature([], tf.string),
        'action_label': tf.io.FixedLenFeature([], tf.string),
        'action_id': tf.io.FixedLenFeature([], tf.int64)    
    }

    parsed_example = tf.io.parse_single_example(example, feature_description)

    video_name=tf.cast(parsed_example['video_name'],tf.string)
    action_label=tf.cast(parsed_example['action_label'],tf.string)
    action_id=tf.cast(parsed_example['action_id'],tf.int64)
    shape=tf.cast(parsed_example['shape'],tf.int64)

    flow_shape=tf.cast(parsed_example['flow_shape'],tf.int64)
    flow=tf.reshape(tf.cast(parsed_example['flow'],tf.float32),flow_shape)

    num_frames=tf.cast(shape[0],tf.float32)
    idx=tf.linspace(0.0,num_frames-1.0,num=tf.cast(num_frames,tf.int32))
    idx=tf.cast(idx,tf.int64)

    frames=tf.map_fn(lambda i: tf.io.decode_png(parsed_example['frames'].values[i]),idx,dtype=tf.uint8)
    fg_mask=tf.map_fn(lambda i: tf.io.decode_png(parsed_example['fg_mask'].values[i]),idx,dtype=tf.uint8)
    
    return (frames,flow,fg_mask,action_id,video_name,action_label)

def fix_dataset(tfrecords_dir,output_dir,new_ids,overwrite=False):
    for split in os.listdir(tfrecords_dir):
        print('-'*50)
        print('processing ',split,' split')
        
        split_dir=os.path.join(output_dir,split)
        if(not os.path.exists(split_dir)):
            os.makedirs(split_dir)
        
        records=[os.path.join(tfrecords_dir,split,r) for r in os.listdir(os.path.join(tfrecords_dir,split)) if r.endswith('tfrecords') ]
        records.sort(key=lambda x: int(x.rsplit('_',2)[1]))

        for idx in range(0, len(records)):
            print('--'*10)

            block=os.path.basename(records[idx])
            tfrecord_file=os.path.join(split_dir,block)
            
            print(tfrecord_file)    
            if(os.path.exists(tfrecord_file) and overwrite == False):
                print('{} already exists, skipping'.format(tfrecord_file))
                continue
            g=tf.Graph()
            with g.as_default():
                dataset=tf.data.TFRecordDataset(records[idx]).map(parse_videoinfo)
                it=dataset.make_one_shot_iterator().get_next()
                count=0
                with tf.io.TFRecordWriter(tfrecord_file) as writer:
                    with tf.Session() as sess:
                        while True:
                            try:
                                frames,flow,fg_mask,action_id,video_name,action_label=sess.run(it)
                            except tf.errors.OutOfRangeError:
                                break
                            
                            action_label=action_label.decode('utf-8')
                            if(not new_ids.get(action_label) is None):
                                print(count,str(video_name),new_ids[action_label],action_label,frames.shape,fg_mask.shape,flow.shape)
                                count=count+1
                                tfexample=get_tfexample(frames,str(action_label),new_ids[action_label],str(video_name),flow,fg_mask).SerializeToString()    
                                writer.write(tfexample)
                            else:
                                print('Excluding {} of class {}'.format(video_name,action_label))

    print('operation completed, tfrecord files saved in ',output_dir)


if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='Neural Network for Action Recogntion from eLR videos')
    parser.add_argument('origin_dir', help='Folder in which the orignal dataset is stored')
    parser.add_argument('output_dir', help='name of the dataset')
    parser.add_argument('--overwrite',default=False,nargs='?',const=True,help='overwrite files in output dir if already exists')
    
    args = parser.parse_args()

    train_test_splits=load_dump(os.path.join(args.origin_dir,'splits.pkl'))

    ids={}
    for el in train_test_splits:
        print(el[0],len(el[1]))
        if('train' in el[0]):
            for video,action_name,action_id in el[1]:
                if(ids.get(action_name) is None):
                    ids[action_name] = [action_id,0]
        
                ids[action_name][1]+=1


    print('n° orignal ids: ',len(ids))
    ids={k:e for k,e in ids.items() if e[1]>=100}
    print('n° ids with 100+ videos: ',len(ids))
    
    new_ids={}

    sorted_keys=list(ids.keys())
    sorted_keys.sort()

    for i,action_name in enumerate(sorted_keys):
        if(ids[action_name][1]>=50):
            new_ids[action_name]=i

    print('n° new ids: ',len(ids))

    tot=0
    
    for k in sorted_keys:
        print(k,ids[k][0],'-->',new_ids[k],' num videos: ',ids[k][1])
        tot+=ids[k][1]

    print('tot: ',tot)

    tot_val=0
    validation=train_test_splits[-1]
    for video,action_name,action_id in validation[1]:
        if(not new_ids.get(action_name) is None):
            tot_val+=1

    print('tot_val: ',tot_val)
    fix_dataset(os.path.join(args.origin_dir,'LR'),os.path.join(args.output_dir,'LR'),new_ids,overwrite=args.overwrite)
    fix_dataset(os.path.join(args.origin_dir,'HR'),os.path.join(args.output_dir,'HR'),new_ids,overwrite=args.overwrite)
