import matplotlib.pyplot as plt
import os
import numpy as np
import pickle as pkl
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()


def save_results(results,dir):
    with open(os.path.join(dir,'results.csv'),mode='w') as file:
        file.write(';'.join(['video','action label','prediction label','action id','prediction','acc','\n']))
        action_dict={}
        for el in results:
            if(action_dict.get(el[2]) is None):
                action_dict[el[2]]=el[1]
        print(action_dict)

        results.sort(key=lambda v:int(v[2]))
        for video in results:
            video.insert(2,action_dict[video[3]])

            row=';'.join(video)
            file.write(row+'\n')

    y_true=[int(el[3]) for el in results]
    y_pred=[int(el[4]) for el in results]
    print(y_true[0],y_pred[0])

    classes=list(action_dict.items())
    classes.sort(key=lambda x:int(x[0]))
    print(classes)
    labels=[name for id,name in classes]
    print(labels)
    
    plot_confusion_matrix(y_true,y_pred,labels,save_path=os.path.join(dir,'confusion_matrix.png')) 

def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues,
                          save_path='confusion_matrix.png'):
        print(len(y_true),len(y_pred))

        confusion_matrix=np.zeros((len(classes),len(classes)),dtype=np.int32)
        
        for i in range(len(y_true)):
            confusion_matrix[y_true[i]][y_pred[i]]+=1

        fig, ax = plt.subplots(figsize=(20.0,20.0))
        im = ax.imshow(confusion_matrix, interpolation='nearest', cmap=cmap)
        ax.figure.colorbar(im, ax=ax)
        
        # We want to show all ticks...
        ax.set(xticks=np.arange(confusion_matrix.shape[1]),
            yticks=np.arange(confusion_matrix.shape[0]),
            # ... and label them with the respective list entries
            xticklabels=classes, yticklabels=classes,
            title=title,
            ylabel='True label',
            xlabel='Predicted label')

        # Rotate the tick labels and set their alignment.
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
                rotation_mode="anchor")

        # Loop over data dimensions and create text annotations.
        fmt = '.2f' if normalize else 'd'
        thresh = confusion_matrix.max() / 2.
        for i in range(confusion_matrix.shape[0]):
            for j in range(confusion_matrix.shape[1]):
                ax.text(j, i, format(confusion_matrix[i, j], fmt),
                        ha="center", va="center",
                        color="white" if confusion_matrix[i, j] > thresh else "black")
        fig.tight_layout()
        plt.savefig(save_path)
        return ax

def add_summary(writer,name, value, global_step):
    summary = tf.Summary(value=[tf.Summary.Value(tag=name, simple_value=value)])
    writer.add_summary(summary,global_step=global_step)
    print(' ',name,': ',str(value))

def load_dump(path):
    if(os.path.exists(path)):
        obj=pkl.load(open(path,"rb"))
        return obj
    else:
        raise FileNotFoundError('Error, file not found')       

