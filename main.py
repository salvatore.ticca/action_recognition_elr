import os
from config import Config
from MSN import MSN
from tfrecords_utils import prepare_dataset
import argparse
from train import *
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Neural Network for Action Recogntion from eLR videos')
    parser.add_argument('dataset_dir', help='Folder in whihc the dataset is stored')
    parser.add_argument('dataset', help='name of the dataset')
    parser.add_argument('logs_dir',help='folder where to save the logs files and the checkpoints')
    parser.add_argument('--rgb_stream',default=False,type=bool,nargs='?',const=True,help='Wheter to use the rgb stream to process RGB')
    parser.add_argument('--optical_flow_stream',default=False,type=bool,nargs='?',const=True,help='Wheter to use the flow stream to process the optical flow')
    parser.add_argument('--mask_stream',default=False,type=bool,nargs='?',const=True,help='Wheter to use the mask stream to process the foreground mask')
    parser.add_argument('--fusion',default='sum',help='specify the fusion strategy for the streams')
    parser.add_argument('--continue_training',default=None,help='if possible restore training from a checkpoint')
    parser.add_argument('--verbose',default=False,nargs='?',const=True,help='if possible restore training from a checkpoint')
    parser.add_argument('--save_freq',default=1,type=int,help='Number of training epochs after which a checkpoint is saved')
    parser.add_argument('--eval_freq',default=5,type=int,help='Number of training epochs after which the model is evaluated')
    parser.add_argument('--num_epochs',default=80,type=int,help='Number of training epochs')
    parser.add_argument('--batch_size',default=64,type=int,help='Batch size')
    parser.add_argument('--gpu',default=0,type=int,help='GPU to use')
    parser.add_argument('--multiplicator',default=1,type=int,help='multiplicator of network weights number')
    parser.add_argument('--shuffle_buffer',default=4000,type=int,help='dimenison of the shuffle buffer')
    parser.add_argument('--start_lr',default=1e-4,type=float,help='initial value of the learning rate')
    parser.add_argument('--end_lr',default=1e-6,type=float,help='final value of the learning rate')
    parser.add_argument('--drp_conv',default=0.0,type=float,help='dropout for conv layers')
    parser.add_argument('--drp_fc1',default=0.0,type=float,help='fc1 dropout')
    parser.add_argument('--drp_fc2',default=0.0,type=float,help='fc2 dropout')
    parser.add_argument('--device_verbose',default=False,nargs='?',const=True,help='log device placement')
    parser.add_argument('--no_crop',default=False,type=bool,nargs='?',const=True,help='Wheter to use the random crop to augment the data')
    parser.add_argument('--inference_time',default=False,type=bool,nargs='?',const=True,help='if specifed run a reduced execution on the test set, to measure the average inference time on a single clip')
    
    args = parser.parse_args()

    config=Config(args.dataset,
                    args.rgb_stream,
                    args.optical_flow_stream,
                    args.mask_stream,
                    args.fusion,
                    args.batch_size,
                    args.multiplicator,
                    crop=not args.no_crop)
        
    print("Num GPUs Available: ", len(tf.config.experimental.list_physical_devices('GPU')))
    if(len(tf.config.experimental.list_physical_devices('GPU')) < 1 ):
        print('No GPU available for training. Interrupting the script...')
        exit()

    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        # Restrict TensorFlow to only use the first GPU
        try:
            tf.config.experimental.set_visible_devices(gpus[args.gpu], 'GPU')
            tf.config.experimental.set_memory_growth(gpus[args.gpu], True)
            logical_gpus = tf.config.experimental.list_logical_devices('GPU')
            print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPU",'SELECTED GPU: ',gpus[args.gpu])
        except RuntimeError as e:
            # Visible devices must be set before GPUs have been initialized
            print(e)
    
    tf.random.set_random_seed(42)
    input_shape=[None,config.INPUT_FRAMES,config.HR_RES[1],config.HR_RES[0],3]
    
    print('Initializing model...')
    model=MSN(config)
  
    print('model creation completed!')

    print('Preparing dataset...')

    #prepare the datset and iterators
    shuffle_dim=min(args.shuffle_buffer,config.DATASET_SIZE[0])
    
    trainset_lr = prepare_dataset(record_folders(args.dataset,args.dataset_dir,'LR',training=True,group_out=config.GROUP_OUT),input_frames=config.INPUT_FRAMES,
                                                    shuffle_buffer=shuffle_dim,repeat=True,batch_size=config.BATCH_SIZE,flip_prob=0.5,
                                                    randomize=config.RANDOMIZE_FRAMES,crop_size=config.CROP_SIZE).make_one_shot_iterator().get_next()

    trainset_hr = prepare_dataset(record_folders(args.dataset,args.dataset_dir,'HR',training=True,group_out=config.GROUP_OUT),input_frames=config.INPUT_FRAMES,
                                                    shuffle_buffer=shuffle_dim,repeat=True,batch_size=config.BATCH_SIZE,flip_prob=0.5,
                                                    randomize=config.RANDOMIZE_FRAMES,crop_size=config.CROP_SIZE).make_one_shot_iterator().get_next()
    
    valset = prepare_dataset(record_folders(args.dataset,args.dataset_dir,'LR',training=False,group_out=config.GROUP_OUT),input_frames=-1,
                                    shuffle_buffer=1,repeat=True,batch_size=0,flip_prob=0.0,randomize=False,crop_size=None).make_one_shot_iterator().get_next()
    
    dataset=(trainset_lr,trainset_hr,valset)

    print('dataset loaded')

    print('-'*50)
    
     
    #compute the number of traing steps to perform t each epoch, that is,
    #the number of steps required to feed the network the whole dataset.
    #(for performance reasons the datast is repeated instead of using a one_shot iterator)
    max_epoch_steps=(2*math.ceil(config.DATASET_SIZE[0]/config.BATCH_SIZE),config.DATASET_SIZE[1])
    print('epochs updates: ',max_epoch_steps)
    print(args.inference_time)

    #if a valid checkpoint is specified restart training from where it stopped
    if(args.continue_training is None):
        if(not os.path.exists(args.logs_dir)):
            os.makedirs(args.logs_dir)
                
        test_id=len(os.listdir(args.logs_dir))
        logs_dir=os.path.join(args.logs_dir,'test_{}'.format(test_id))
        continue_training=False
    else:
        logs_dir=os.path.join(args.logs_dir,args.continue_training)
        continue_training=True

        print(logs_dir)   
    
    if(not args.inference_time):
        #train the model
        train_sc(model,
                dataset=dataset,
                max_epoch_steps=max_epoch_steps,
                logs_dir=logs_dir,
                num_epochs=args.num_epochs,
                continue_training=continue_training,
                save_freq=args.save_freq,
                eval_freq=args.eval_freq,
                start_lr=args.start_lr,
                end_lr=args.end_lr,
                verbose=args.verbose,
                drp_fc1=args.drp_fc1,
                drp_fc2=args.drp_fc2,
                drp_conv=args.drp_conv,
                device_verbose=args.device_verbose)
    else:
        #measure the inference time required to evaluate a single batch
        measure_inference_time(model,
                                dataset=dataset,
                                logs_dir=logs_dir,
                                continue_training=continue_training,
                                max_epoch_steps=max_epoch_steps)
